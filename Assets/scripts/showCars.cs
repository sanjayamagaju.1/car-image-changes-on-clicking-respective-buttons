using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showCars : MonoBehaviour
{
    public GameObject default_display, car1_display, car2_display, car3_display, car4_display;
    public Button car1, car2, car3, car4;
    // Start is called before the first frame update
    void Start()
    {
        car1_display.SetActive(false);
        car2_display.SetActive(false);
        car3_display.SetActive(false);
        car4_display.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void showCar1()
    {
        default_display.SetActive(false);
        car1_display.SetActive(true);
        car2_display.SetActive(false);
        car3_display.SetActive(false);
        car4_display.SetActive(false);
    }

    public void showCar2()
    {
        default_display.SetActive(false);
        car1_display.SetActive(false);
        car2_display.SetActive(true);
        car3_display.SetActive(false);
        car4_display.SetActive(false);
    }

    public void showCar3()
    {
        default_display.SetActive(false);
        car1_display.SetActive(false);
        car2_display.SetActive(false);
        car3_display.SetActive(true);
        car4_display.SetActive(false);
    }

    public void showCar4()
    {
        default_display.SetActive(false);
        car1_display.SetActive(false);
        car2_display.SetActive(false);
        car3_display.SetActive(false);
        car4_display.SetActive(true);
    }
}
